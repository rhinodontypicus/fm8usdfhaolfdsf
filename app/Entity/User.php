<?php

namespace App\Entity;

use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableInterface;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements AuthenticatableInterface
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'is_admin',
        'email',
        'password'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'is_admin' => 'boolean',
    ];

    public function cars()
    {
        return $this->hasMany(Car::class);
    }

    /**
     * @return mixed
     */
    public function isAdmin()
    {
        return $this->attributes['is_admin'];
    }
}
