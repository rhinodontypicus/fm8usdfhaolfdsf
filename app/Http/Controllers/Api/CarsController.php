<?php

namespace App\Http\Controllers\Api;

use App\Entity\Car;
use App\Http\Controllers\Controller;
use App\Http\Requests\CarRequest;
use Illuminate\Auth\Access\AuthorizationException;

class CarsController extends Controller
{
    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function index()
    {
        return Car::all();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        $car = Car::findOrFail($id);

        return $car;
    }

    /**
     * @param $id
     * @param CarRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, CarRequest $request)
    {
        $car = Car::findOrFail($id);

        try {
            $this->authorize('update', $car);
        } catch (AuthorizationException $e) {
            return redirect('/');
        }

        $car->update($request->all());

        return $car;
    }

    /**
     * @param CarRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(CarRequest $request)
    {
        try {
            $this->authorize('create', Car::class);
        } catch (AuthorizationException $e) {
            return redirect('/');
        }

        $car = Car::create($request->all());

        return $car;
    }

    /**
     * @param $id
     * @return array|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $car = Car::findOrFail($id);

        try {
            $this->authorize('delete', $car);
        } catch (AuthorizationException $e) {
            return redirect('/');
        }

        $car->delete();

        return ['success' => true];
    }
}
