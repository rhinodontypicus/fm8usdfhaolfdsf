<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::name('api.')->middleware('auth')->group(function () {
    Route::resource('cars', 'Api\CarsController', [
        'only' => [
            'index',
            'show',
        ],
    ]);

    Route::prefix('admin')->name('admin.')->middleware('admin')->group(function () {
        Route::resource('cars', 'Api\CarsController', [
            'only' => [
                'index',
                'store',
                'show',
                'update',
                'destroy',
            ],
        ]);
    });
});
