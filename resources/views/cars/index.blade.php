@extends('layouts.app')

@section('content')
    <div class="list-group">
        @forelse($cars as $car)
            <a class="list-group-item" href="{{ route('cars.show', $car['id']) }}">
                {{ $car['model'] }} [{{ $car['color'] }}] — ${{ $car['price'] }}
            </a>
        @empty
            <p class="list-group-item">No cars</p>
        @endforelse
    </div>
@endsection