{!! csrf_field() !!}

<div class="form-group @if ($errors->has('model')) has-error @endif">
    <label for="model">Model</label>
    <input value="{{ old('model') ?? $car['model'] }}" type="text" class="form-control" name="model" id="model" placeholder="Model">

    @if ($errors->has('model'))
        <span class="help-block">{{ $errors->first('model') }}</span>
    @endif
</div>

<div class="form-group @if ($errors->has('registration_number')) has-error @endif">
    <label for="registration_number">Registration Number</label>
    <input value="{{ old('registration_number') ?? $car['registration_number'] }}" type="text" class="form-control" name="registration_number" id="registration_number" placeholder="Registration Number">

    @if ($errors->has('registration_number'))
        <span class="help-block">{{ $errors->first('registration_number') }}</span>
    @endif
</div>

<div class="form-group @if ($errors->has('year')) has-error @endif">
    <label for="year">Year</label>
    <input value="{{ old('year') ?? $car['year'] }}" type="text" class="form-control" name="year" id="year" placeholder="Year">

    @if ($errors->has('year'))
        <span class="help-block">{{ $errors->first('year') }}</span>
    @endif
</div>

<div class="form-group @if ($errors->has('color')) has-error @endif">
    <label for="color">Color</label>
    <input value="{{ old('color') ?? $car['color'] }}" type="text" class="form-control" name="color" id="color" placeholder="Color">

    @if ($errors->has('color'))
        <span class="help-block">{{ $errors->first('color') }}</span>
    @endif
</div>

<div class="form-group @if ($errors->has('price')) has-error @endif">
    <label for="price">Price</label>
    <input value="{{ old('price') ?? $car['price'] }}" type="text" class="form-control" name="price" id="price" placeholder="Price">

    @if ($errors->has('price'))
        <span class="help-block">{{ $errors->first('price') }}</span>
    @endif
</div>

<button type="submit" class="btn btn-default">Save</button>